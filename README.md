# NAME
## Piano Software, Inc.

---

### Overview
Implement internal source control within front-end template development. A good way to guarantee versioning. Control template updates locally and push to Piano Template Manager on your cadence.

- Manage template settings
- Create new templates
- Update existing templates
- Update existing content fields
- Utilize Piano Developer API

Allows multi-site publishers to contain all templates from multiple applications under one global control. This allows multiple developers to be working within the same code simultaneously. Script logic ensures only templates that have changes will be updated in their respective application Library.

Use of CI/CD (Continuous Integration/Continuous Delivery) allows for automated publishing through the Piano Developer API. Sharing code in a merge request triggers the CI/CD pipeline.
Once that code is validated, changes are pushed to the API via the structured deployment pipelines.

### Getting Started

Clone the repository:

> Please note there will be secret information within your project, use private repo

`https://gitlab.com/piano-public/client-technology/templates-ci-cd-public.git`

Run the script that compiles the project:

`scripts/api-piano.sh`

Update folders to match your individual needs. For multi-site publishers, create a sub-folder of `publisher` to reflect each AID property. Within your AID folder(s), add a sub-folder for each existing template. Each template's folder should contain:
- `content-field-list.js`
- `template-[name].html`
- `template-[name].css`
- `var.sh`
   - This variable shell script contains pertinent template manager information such as:
      - Name
      - AID
      - API Token
      - Category ID (template type)
      - Content 1 Type (html or css)
      - Content 2 Type (html or css)
      - The values of Content 1 & 2
      - Template ID
      - Intended Action:

      > Allowed values are "save", "new_version_only", "new_version_and_publish"

      - API URL
   - Other variables are available within [docs](https://docs.piano.io/api/) under `/publisher/offer/template/`. Please note any additional variables added to `var.sh` will need to be added to the curl string within `api-piano.sh` to be properly pushed.
