"content_field_list" : [ {
    "content_field_id" : "OTCFCLJZ10S4E2",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "header",
    "description" : "Header",
    "deleted" : false,
    "value" : "Hello world"
  }, {
    "content_field_id" : "OTCFRLHEOF60B8",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "subHeader",
    "description" : "Subheader",
    "deleted" : false,
    "value" : "Click below to checkout"
  }, {
    "content_field_id" : "OTCF9YXL0WDOI7",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "callToAction",
    "description" : "CTA",
    "deleted" : false,
    "value" : "Checkout"
  }, {
    "content_field_id" : "OTCF2V8KAX95AL",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "headerImage",
    "description" : "Header image",
    "deleted" : false,
    "value" : ""
  }, {
    "content_field_id" : "OTCF0SEYSUI5MW",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "callToActionColor",
    "description" : "Button color",
    "deleted" : false,
    "value" : "#3b67b2"
  }, {
    "content_field_id" : "OTCFY3HP9PJ4I4",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "callToActionTextColor",
    "description" : "Button text color",
    "deleted" : false,
    "value" : "#ffffff"
  }, {
    "content_field_id" : "OTCFVZ2GAH1GM9",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "backgroundColor",
    "description" : "Background color",
    "deleted" : false,
    "value" : "#ffffff"
  }, {
    "content_field_id" : "OTCF3IVEYS75T8",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "headerTextColor",
    "description" : "Header text color",
    "deleted" : false,
    "value" : "#323232"
  }, {
    "content_field_id" : "OTCFGSKFUUF1W5",
    "offer_template_variant_id" : null,
    "offer_template_id" : "OTL1TQ5SOCKK",
    "name" : "subheaderTextColor",
    "description" : "Subheader text color",
    "deleted" : false,
    "value" : "#323232"
  } ]
