#!/bin/bash

# Find only newly changed doc files
git fetch origin

templatesDiff=$(git diff --dirstat=files,0 HEAD~1 | sed -E 's/^[ 0-9.]+% //g')

for template in $templatesDiff; do
    valid='^publisher\/\w*'
    if [[ $template =~ $valid ]];
    then
        echo "Template changed: '$template'"
        source "${template}/var.sh"
        export content1_value="$(cat ${template}/${content1_value})"
        export content2_value="$(cat ${template}/${content2_value})"

        curl -X POST --header "Content-Type: application/x-www-form-urlencoded" --header "Accept: application/json" -d "aid=${aid}&api_token=${api_token}&category_id=${category_id}&content1_type=${content1_type}&content1_value=${content1_value}&content2_type=${content2_type}&content2_value=${content2_value}&name=${name}&offer_template_id=${offer_template_id}&action=${action}" "${api_url}"
    fi
done
